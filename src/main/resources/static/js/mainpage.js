$(document).ready(function () {
    $('#departure').val(formatDate(+new Date()));
    $("#numberOfTravellers").val("1");

    $("#searchFlight-form").submit(function (event) {
        $("#src-label").empty();
        $("#dest-label").empty();
        if(jQuery.trim($("#source").val()).length <= 0) {
            $('#src-label').append('*Source can not be blank');
        }
        if(jQuery.trim($("#destination").val()).length <= 0) {
            $('#dest-label').append('*Destination can not be blank');
        }

        //stop submit the form, we will post it manually.
        event.preventDefault();
        fire_ajax_submit();
    });
});

var headers = [
    {
        "name": "Flight Name",
        "key": "name"
    },
    {
        "name": "Tickets",
        "key": "numOfTickets"
    },
    {
        "name": "Total Fare",
        "key": "totalFare"
    },
];

function clearFlightsTable() {
    $("#resultsTable").empty();
}

function renderFlightsTable(flightsData, topHeaders) {
    const container = $("<div></div>").addClass("card column flightTable");
    if (flightsData.length > 0) {
        topHeaders.forEach(function (header) {
           container.append($("<h4 class=\"title is-4\"></h4>").html(header));
        });

        const label = $("<h6 class=\"subtitle is-6\"></h6>").html(flightsData[0]["source"] + " -> " + flightsData[0]['destination']);
        container.append(label);
        container.append("<hr>");
        const table = $("<table></table>").addClass("is-striped is-fullwidth table");
        const thead = $("<thead></thead>");
        const row = $("<tr></tr>");
        headers.forEach(obj => row.append($("<th></th>").html(obj.name)));
        thead.append(row);

        const tbody = $("<tbody></tbody>");
        flightsData.forEach(flightData => {
            const row = $("<tr></tr>");
            headers.forEach(obj => {
               if (typeof obj.key === 'string' || obj.key instanceof String) {
                   row.append($("<td></td>").html(flightData[obj.key]));
               }
            });

            tbody.append(row);
        });

        table.append(thead);
        table.append(tbody);

        container.append(table);
    } else {
        container.append($("<h4></h4>").html("No flights available"));
    }
    $("#resultsTable").append(container);
}

function fire_ajax_submit() {
    // $("#btn-search").prop("disabled", true);

    $.ajax({
        type: "GET",
        contentType: "application/json",
        url: "/flights/search",
        data: {
            src: $("#source").val(),
            des: $("#destination").val(),
            tickets: $("#numberOfTravellers").val(),
            classType: $("#classType").prop('selectedIndex'),
            departureDate: $("#departure").val(),
            returnDate: $("#return").val()
        },
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {
            clearFlightsTable();
            Object.keys(data).forEach(function (key) {
                const headers = [];
                if (key === "onwards") {
                    headers.push("Departure Flight");
                } else {
                    headers.push("Return Flight");
                }

                renderFlightsTable(data[key], headers);
            });
            var json = "<h4>Ajax Response</h4><pre>"
                + JSON.stringify(data, null, 4) + "</pre>";
            $('#searchFlight').html(json);

            console.log("SUCCESS : ", data);
            // $("#btn-search").prop("disabled", false);

        },
        error: function (e) {

            var json = "<h4>Ajax Response</h4><pre>"
                + e.responseText + "</pre>";
            $('#feedback').html(json);

            console.log("ERROR : ", e);
            $("#btn-search").prop("disabled", false);
        }
    });
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
