package com.thoughtworks.vapasi.flightbookingapp;

public class Constants {
  public static final String DATE_FORMAT = "dd/MM/yyyy";
  public static final String JSON_DATE_FORMAT = "yyyy-MM-dd";

}
