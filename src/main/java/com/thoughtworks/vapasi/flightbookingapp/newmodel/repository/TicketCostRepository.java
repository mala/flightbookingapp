package com.thoughtworks.vapasi.flightbookingapp.newmodel.repository;

import com.thoughtworks.vapasi.flightbookingapp.newmodel.model.TicketCost;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TicketCostRepository implements ObjectRepository<TicketCost>{

  private Map<Integer, TicketCost> repository;

  public TicketCostRepository() {
    this.repository = new HashMap<>();
  }

  @Override
  public void store(TicketCost ticketCost) {
    repository.put(ticketCost.getId(), ticketCost);

    this.store(new TicketCost(1,1, 1, 9000));
    this.store(new TicketCost(2,2, 1, 20000));
    this.store(new TicketCost(3,3, 1, 80000));

    this.store(new TicketCost(4,1, 2, 10000));
    this.store(new TicketCost(5,2, 2, 25000));
    this.store(new TicketCost(6,3, 2, 95000));

    this.store(new TicketCost(7,1, 3, 7000));
    this.store(new TicketCost(8,2, 3, 18000));
    this.store(new TicketCost(9,3, 3, 70000));

    this.store(new TicketCost(10,1, 4, 8000));
    this.store(new TicketCost(11,2, 4, 19000));
    this.store(new TicketCost(12,3, 4, 78000));
  }

  @Override
  public TicketCost retrieve(int id) {
    return repository.get(id);
  }

  @Override
  public TicketCost search(String name) {
    return null;
  }

  @Override
  public TicketCost delete(int id) {
    TicketCost ticketCost = repository.get(id);
    this.repository.remove(id);
    return ticketCost;
  }

  @Override
  public List getAll() {
    return new ArrayList<>(repository.values());
  }
}
