package com.thoughtworks.vapasi.flightbookingapp.newmodel.model;

public class AvailableSeats {
  private int id;
  private int scheduledFlightId;
  private int seatTypeId;
  private int availableSeats;

  public AvailableSeats(int id, int scheduledFlightId, int seatTypeId, int availableSeats) {
    this.id = id;
    this.scheduledFlightId = scheduledFlightId;
    this.seatTypeId = seatTypeId;
    this.availableSeats = availableSeats;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getScheduledFlightId() {
    return scheduledFlightId;
  }

  public void setScheduledFlightId(int scheduledFlightId) {
    this.scheduledFlightId = scheduledFlightId;
  }

  public int getSeatTypeId() {
    return seatTypeId;
  }

  public void setSeatTypeId(int seatTypeId) {
    this.seatTypeId = seatTypeId;
  }

  public int getAvailableSeats() {
    return availableSeats;
  }

  public void setAvailableSeats(int availableSeats) {
    this.availableSeats = availableSeats;
  }
}
