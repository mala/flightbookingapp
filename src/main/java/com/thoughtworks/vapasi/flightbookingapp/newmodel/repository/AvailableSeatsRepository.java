package com.thoughtworks.vapasi.flightbookingapp.newmodel.repository;

import com.thoughtworks.vapasi.flightbookingapp.newmodel.model.AvailableSeats;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AvailableSeatsRepository implements ObjectRepository<AvailableSeats> {

  private Map<Integer, AvailableSeats> repository;

  public AvailableSeatsRepository() {
    this.repository = new HashMap<>();

    this.store(new AvailableSeats(1, 1, 1, 40));
    this.store(new AvailableSeats(2, 1, 2, 20));
    this.store(new AvailableSeats(3, 1, 3, 10));

    this.store(new AvailableSeats(4, 2, 1, 100));
    this.store(new AvailableSeats(5, 2, 2, 30));
    this.store(new AvailableSeats(6, 2, 3, 10));

    this.store(new AvailableSeats(7, 3, 1, 20));
    this.store(new AvailableSeats(8, 3, 2, 30));
    this.store(new AvailableSeats(9, 3, 3, 5));

    this.store(new AvailableSeats(10, 4, 1, 40));
    this.store(new AvailableSeats(11, 4, 2, 23));
    this.store(new AvailableSeats(12, 4, 3, 5));

    this.store(new AvailableSeats(13, 5, 1, 55));
    this.store(new AvailableSeats(14, 5, 2, 15));
    this.store(new AvailableSeats(15, 5, 3, 7));

    this.store(new AvailableSeats(16, 6, 1, 70));
    this.store(new AvailableSeats(17, 6, 2, 16));
    this.store(new AvailableSeats(18, 6, 3, 0));

    this.store(new AvailableSeats(19, 7, 1, 48));
    this.store(new AvailableSeats(20, 7, 2, 21));
    this.store(new AvailableSeats(21, 7, 3, 4));

    this.store(new AvailableSeats(22, 8, 1, 78));
    this.store(new AvailableSeats(23, 8, 2, 17));
    this.store(new AvailableSeats(24, 8, 3, 4));

    this.store(new AvailableSeats(25, 9, 1, 12));
    this.store(new AvailableSeats(26, 9, 2, 27));
    this.store(new AvailableSeats(27, 9, 3, 7));

    this.store(new AvailableSeats(28, 10, 1, 135));
    this.store(new AvailableSeats(29, 10, 2, 26));
    this.store(new AvailableSeats(30, 10, 3, 4));

    this.store(new AvailableSeats(31, 11, 1, 93));
    this.store(new AvailableSeats(32, 11, 2, 6));
    this.store(new AvailableSeats(33, 11, 3, 0));

    this.store(new AvailableSeats(34, 12, 1, 58));
    this.store(new AvailableSeats(35, 12, 2, 28));
    this.store(new AvailableSeats(36, 12, 3, 0));
  }
  @Override
  public void store(AvailableSeats availableSeats) {
    repository.put(availableSeats.getId(), availableSeats);
  }

  @Override
  public AvailableSeats retrieve(int id) {
    return repository.get(id);
  }

  @Override
  public AvailableSeats search(String name) {
    return null;
  }

  @Override
  public AvailableSeats delete(int id) {
    AvailableSeats availableSeats = repository.get(id);
    this.repository.remove(id);
    return availableSeats;
  }

  @Override
  public List getAll() {
    return new ArrayList<>(repository.values());
  }
}
