package com.thoughtworks.vapasi.flightbookingapp.newmodel.repository;

import com.thoughtworks.vapasi.flightbookingapp.newmodel.model.TravelClass;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TravelClassRepository implements ObjectRepository<TravelClass> {

  private Map<Integer, TravelClass> repository;

  public TravelClassRepository() {
    this.repository = new HashMap<>();

    this.store(new TravelClass(1,1, 1, 100));
    this.store(new TravelClass(2,1, 2, 30));
    this.store(new TravelClass(3,1, 3, 10));

    this.store(new TravelClass(4,2, 1, 100));
    this.store(new TravelClass(5,2, 2, 40));
    this.store(new TravelClass(6,2, 3, 10));

    this.store(new TravelClass(7,3, 1, 200));
    this.store(new TravelClass(8,3, 2, 0));
    this.store(new TravelClass(9,3, 3, 10));

    this.store(new TravelClass(10,4, 1, 190));
    this.store(new TravelClass(11,4, 2, 30));
    this.store(new TravelClass(12,4, 3, 10));

    this.store(new TravelClass(13,5, 1, 130));
    this.store(new TravelClass(14,5, 2, 30));
    this.store(new TravelClass(15,5, 3, 10));

    this.store(new TravelClass(16,6, 1, 150));
    this.store(new TravelClass(17,6, 2, 30));
    this.store(new TravelClass(18,6, 3, 10));

    this.store(new TravelClass(19,7, 1, 100));
    this.store(new TravelClass(20,7, 2, 30));
    this.store(new TravelClass(21,7, 3, 10));

    this.store(new TravelClass(22,8, 1, 100));
    this.store(new TravelClass(23,8, 2, 30));
    this.store(new TravelClass(24,8, 3, 10));

    this.store(new TravelClass(25,9, 1, 120));
    this.store(new TravelClass(26,9, 2, 30));
    this.store(new TravelClass(27,9, 3, 10));

    this.store(new TravelClass(28,10, 1, 120));
    this.store(new TravelClass(29,10, 2, 30));
    this.store(new TravelClass(30,10, 3, 5));

    this.store(new TravelClass(31,11, 1, 100));
    this.store(new TravelClass(32,11, 2, 30));
    this.store(new TravelClass(33,11, 3, 10));

    this.store(new TravelClass(34,12, 1, 150));
    this.store(new TravelClass(35,12, 2, 30));
    this.store(new TravelClass(36,12, 3, 10));

    this.store(new TravelClass(37,13, 1, 110));
    this.store(new TravelClass(38,13, 2, 10));
    this.store(new TravelClass(39,13, 3, 0));

    this.store(new TravelClass(40,14, 1, 100));
    this.store(new TravelClass(41,14, 2, 30));
    this.store(new TravelClass(42,14, 3, 10));

    this.store(new TravelClass(43,15, 1, 70));
    this.store(new TravelClass(44,15, 2, 50));
    this.store(new TravelClass(45,15, 3, 20));

    this.store(new TravelClass(46,16, 1, 200));
    this.store(new TravelClass(47,16, 2, 0));
    this.store(new TravelClass(48,16, 3, 0));

    this.store(new TravelClass(49,17, 1, 100));
    this.store(new TravelClass(50,17, 2, 30));
    this.store(new TravelClass(51,17, 3, 6));

    this.store(new TravelClass(52,18, 1, 100));
    this.store(new TravelClass(53,18, 2, 30));
    this.store(new TravelClass(54,18, 3, 0));

    this.store(new TravelClass(55,19, 1, 150));
    this.store(new TravelClass(56,19, 2, 10));
    this.store(new TravelClass(57,19, 3, 10));

    this.store(new TravelClass(58,20, 1, 150));
    this.store(new TravelClass(59,20, 2, 10));
    this.store(new TravelClass(60,20, 3, 6));

    this.store(new TravelClass(61,21, 1, 100));
    this.store(new TravelClass(62,21, 2, 30));
    this.store(new TravelClass(63,21, 3, 8));
  }

  @Override
  public void store(TravelClass travelClass) {
    repository.put(travelClass.getId(), travelClass);
  }

  @Override
  public TravelClass retrieve(int id) {
    return repository.get(id);
  }

  @Override
  public TravelClass search(String name) {
    return null;
  }

  @Override
  public TravelClass delete(int id) {
    TravelClass travelClass = repository.get(id);
    this.repository.remove(id);
    return travelClass;
  }

  @Override
  public List<TravelClass> getAll() {
    return new ArrayList<>(repository.values());
  }
}
