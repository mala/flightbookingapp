package com.thoughtworks.vapasi.flightbookingapp.newmodel.repository;

import com.thoughtworks.vapasi.flightbookingapp.newmodel.model.SeatType;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class SeatTypeRepository implements ObjectRepository<SeatType> {

  private Map<Integer, SeatType> repository;

  public SeatTypeRepository() {
    this.repository = new HashMap<>();

    this.store(new SeatType(1,"economy"));
    this.store(new SeatType(2,"first"));
    this.store(new SeatType(3,"business"));
  }

  @Override
  public void store(SeatType seatType) {
    repository.put(seatType.getId(), seatType);
  }

  @Override
  public SeatType retrieve(int id) {
    return repository.get(id);
  }

  @Override
  public SeatType search(String name) {
    return null;
  }

  @Override
  public SeatType delete(int id) {
    SeatType seatType = repository.get(id);
    this.repository.remove(id);
    return seatType;
  }

  @Override
  public List<SeatType> getAll() {
    return new ArrayList<>(repository.values());
  }
}
