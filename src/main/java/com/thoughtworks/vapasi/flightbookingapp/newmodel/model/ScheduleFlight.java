package com.thoughtworks.vapasi.flightbookingapp.newmodel.model;

import java.util.Date;

public class ScheduleFlight {
  private int id;
  private int flightId;
  private int routeId;
  private Date departureDate;

  public ScheduleFlight(int id, int flightId, int routeId, Date departureDate) {
    this.id = id;
    this.flightId = flightId;
    this.routeId = routeId;
    this.departureDate = departureDate;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getFlightId() {
    return flightId;
  }

  public void setFlightId(int flightId) {
    this.flightId = flightId;
  }

  public int getRouteId() {
    return routeId;
  }

  public void setRouteId(int routeId) {
    this.routeId = routeId;
  }

  public Date getDepartureDate() {
    return departureDate;
  }

  public void setDepartureDate(Date departureDate) {
    this.departureDate = departureDate;
  }
}
