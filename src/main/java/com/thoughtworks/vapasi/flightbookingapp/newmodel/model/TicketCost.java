package com.thoughtworks.vapasi.flightbookingapp.newmodel.model;

public class TicketCost {
  private int id;
  private int seatTypeId;
  private int routeId;
  private double price;

  public TicketCost(int id, int seatTypeId, int routeId, double price) {
    this.id = id;
    this.seatTypeId = seatTypeId;
    this.routeId = routeId;
    this.price = price;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getSeatTypeId() {
    return seatTypeId;
  }

  public void setSeatTypeId(int seatTypeId) {
    this.seatTypeId = seatTypeId;
  }

  public int getRouteId() {
    return routeId;
  }

  public void setRouteId(int routeId) {
    this.routeId = routeId;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }
}
