package com.thoughtworks.vapasi.flightbookingapp.newmodel.service;

import com.thoughtworks.vapasi.flightbookingapp.newmodel.model.Route;
import com.thoughtworks.vapasi.flightbookingapp.newmodel.repository.RouteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RouteService {
  private RouteRepository routeRepository;

  @Autowired
  public void setBookingsRepository(RouteRepository routeRepository) {
    this.routeRepository = routeRepository;
  }

  public Route getRouteForId(int id) {
    return routeRepository.retrieve(id);
  }
}
