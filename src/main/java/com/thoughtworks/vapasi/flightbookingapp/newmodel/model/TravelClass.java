package com.thoughtworks.vapasi.flightbookingapp.newmodel.model;

public class TravelClass {
  private int id;
  private int flightId;
  private int seatTypeId;
  private int totalSeats;


  public TravelClass(int id, int flightId, int seatTypeId, int totalSeats) {
    this.id = id;
    this.flightId = flightId;
    this.seatTypeId = seatTypeId;
    this.totalSeats = totalSeats;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getFlightId() {
    return flightId;
  }

  public void setFlightId(int flightId) {
    this.flightId = flightId;
  }

  public int getSeatTypeId() {
    return seatTypeId;
  }

  public void setSeatTypeId(int seatTypeId) {
    this.seatTypeId = seatTypeId;
  }

  public int getTotalSeats() {
    return totalSeats;
  }

  public void setTotalSeats(int totalSeats) {
    this.totalSeats = totalSeats;
  }
}
