package com.thoughtworks.vapasi.flightbookingapp.newmodel.repository;

import com.thoughtworks.vapasi.flightbookingapp.Constants;
import com.thoughtworks.vapasi.flightbookingapp.newmodel.model.ScheduleFlight;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class ScheduleFlightRepository implements ObjectRepository<ScheduleFlight> {

  private Map<Integer, ScheduleFlight> repository;


  @Override
  public void store(ScheduleFlight scheduleFlight) {
    repository.put(scheduleFlight.getId(), scheduleFlight);

    Date dDate = new Date();
    try {
      dDate = new SimpleDateFormat(Constants.DATE_FORMAT).parse("5/09/2019");
    } catch (ParseException e) {
      e.printStackTrace();
    }
    this.store(new ScheduleFlight(1, 1, 1, dDate));
    this.store(new ScheduleFlight(2, 5, 1, dDate));
    this.store(new ScheduleFlight(3, 9, 1, dDate));
    this.store(new ScheduleFlight(4, 10, 2, dDate));
    this.store(new ScheduleFlight(5, 14, 2, dDate));
    this.store(new ScheduleFlight(6, 18, 2, dDate));
    this.store(new ScheduleFlight(7, 21, 3, dDate));
    this.store(new ScheduleFlight(8, 17, 3, dDate));
    this.store(new ScheduleFlight(9, 14, 3, dDate));
    this.store(new ScheduleFlight(10, 6, 4, dDate));
    this.store(new ScheduleFlight(11, 13, 4, dDate));
    this.store(new ScheduleFlight(12, 18, 4, dDate));
  }

  @Override
  public ScheduleFlight retrieve(int id) {
    return repository.get(id);
  }

  @Override
  public ScheduleFlight search(String name) {
    return null;
  }

  @Override
  public ScheduleFlight delete(int id) {
    ScheduleFlight scheduleFlight = repository.get(id);
    this.repository.remove(id);
    return scheduleFlight;
  }

  @Override
  public List<ScheduleFlight> getAll() {
    return new ArrayList<>(repository.values());
  }
}
