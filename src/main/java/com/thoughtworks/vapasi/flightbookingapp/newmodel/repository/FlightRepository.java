package com.thoughtworks.vapasi.flightbookingapp.newmodel.repository;

import com.thoughtworks.vapasi.flightbookingapp.newmodel.model.Flight;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class FlightRepository implements ObjectRepository<Flight> {

  private Map<Integer, Flight> repository;

  public FlightRepository() {
    this.repository = new HashMap<>();

    this.store(new Flight(1,"Air Costa", "Boeing 777- 200LR(77L)"));
    this.store(new Flight(2, "Air Costa", "Airbus A319 V2"));
    this.store(new Flight(3, "Air Costa", "Airbus A321"));

    this.store(new Flight(4, "AirAsia India", "Boeing 777- 200LR(77L)"));
    this.store(new Flight(5, "AirAsia India", "Airbus A319 V2"));
    this.store(new Flight(6, "AirAsia India", "Airbus A321"));

    this.store(new Flight(7, "IndiGo", "Boeing 777- 200LR(77L)"));
    this.store(new Flight(8,"IndiGo","Airbus A319 V2"));
    this.store(new Flight(9,"IndiGo","Airbus A321"));

    this.store(new Flight(10, "Vistara", "Boeing 777- 200LR(77L)"));
    this.store(new Flight(11,"Vistara","Airbus A319 V2"));
    this.store(new Flight(12,"Vistara","Airbus A321"));

    this.store(new Flight(13, "GoAir", "Boeing 777- 200LR(77L)"));
    this.store(new Flight(14,"GoAir","Airbus A319 V2"));
    this.store(new Flight(15,"GoAir","Airbus A321"));

    this.store(new Flight(16, "Jet Airways", "Boeing 777- 200LR(77L)"));
    this.store(new Flight(17, "Jet Airways","Airbus A319 V2"));
    this.store(new Flight(18,"Jet Airways","Airbus A321"));

    this.store(new Flight(19, "SpiceJet", "Boeing 777- 200LR(77L)"));
    this.store(new Flight(20,"SpiceJet","Airbus A319 V2"));
    this.store(new Flight(21,"SpiceJet","Airbus A321"));

  }

  @Override
  public void store(Flight flight) {
    repository.put(flight.getId(), flight);
  }

  @Override
  public Flight retrieve(int id) {
    return repository.get(id);
  }

  @Override
  public Flight search(String name) {
    return null;
  }

  @Override
  public Flight delete(int id) {
    Flight flight = repository.get(id);
    this.repository.remove(id);
    return flight;
  }

  @Override
  public List getAll() {
    return new ArrayList<>(repository.values());
  }
}
