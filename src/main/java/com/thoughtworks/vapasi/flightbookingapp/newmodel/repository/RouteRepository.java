package com.thoughtworks.vapasi.flightbookingapp.newmodel.repository;

import com.thoughtworks.vapasi.flightbookingapp.newmodel.model.Route;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class RouteRepository implements ObjectRepository<Route> {

  private Map<Integer, Route> repository;

  public RouteRepository() {
    this.repository = new HashMap<>();

    this.store(new Route(1,"hyderabad", "delhi"));
    this.store(new Route(2,"delhi", "hyderabad"));
    this.store(new Route(3,"hyderabad", "bangalore"));
    this.store(new Route(4,"bangalore", "hyderabad"));
  }

  @Override
  public void store(Route route) {
    repository.put(route.getId(), route);
  }

  @Override
  public Route retrieve(int id) {
    return repository.get(id);
  }

  @Override
  public Route search(String name) {
    return null;
  }

  @Override
  public Route delete(int id) {
    Route route = repository.get(id);
    this.repository.remove(id);
    return route;
  }

  @Override
  public List<Route> getAll() {
    return new ArrayList<>(repository.values());
  }
}
