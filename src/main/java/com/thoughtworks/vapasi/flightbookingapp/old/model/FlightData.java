package com.thoughtworks.vapasi.flightbookingapp.old.model;

import java.util.Objects;

public class FlightData {
    private int flightId;
    private String flightModel;

    private String source;
    private String destination;

    private int totalSeatsInFirstClass;
    private int totalSeatsInBusinessClass;
    private int totalSeatsInEconomyClass;

    private double baseFareForFirstClass;
    private double baseFareForBusinessClass;
    private double baseFareForEconomyClass;

    public FlightData(int flightId, String flightModel, String source, String destination, int totalSeatsInFirstClass,
                      int totalSeatsInBusinessClass, int totalSeatsInEconomyClass, double baseFareForFirstClass,
                      double baseFareForBusinessClass, double baseFareForEconomyClass) {
        this.flightId = flightId;
        this.flightModel = flightModel;
        this.source = source;
        this.destination = destination;
        this.totalSeatsInFirstClass = totalSeatsInFirstClass;
        this.totalSeatsInBusinessClass = totalSeatsInBusinessClass;
        this.totalSeatsInEconomyClass = totalSeatsInEconomyClass;
        this.baseFareForFirstClass = baseFareForFirstClass;
        this.baseFareForBusinessClass = baseFareForBusinessClass;
        this.baseFareForEconomyClass = baseFareForEconomyClass;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public String getFlightModel() {
        return flightModel;
    }

    public void setFlightModel(String flightModel) {
        this.flightModel = flightModel;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getTotalSeatsInFirstClass() {
        return totalSeatsInFirstClass;
    }

    public void setTotalSeatsInFirstClass(int totalSeatsInFirstClass) {
        this.totalSeatsInFirstClass = totalSeatsInFirstClass;
    }

    public int getTotalSeatsInBusinessClass() {
        return totalSeatsInBusinessClass;
    }

    public void setTotalSeatsInBusinessClass(int totalSeatsInBusinessClass) {
        this.totalSeatsInBusinessClass = totalSeatsInBusinessClass;
    }

    public int getTotalSeatsInEconomyClass() {
        return totalSeatsInEconomyClass;
    }

    public void setTotalSeatsInEconomyClass(int totalSeatsInEconomyClass) {
        this.totalSeatsInEconomyClass = totalSeatsInEconomyClass;
    }

    public double getBaseFareForFirstClass() {
        return baseFareForFirstClass;
    }

    public void setBaseFareForFirstClass(double baseFareForFirstClass) {
        this.baseFareForFirstClass = baseFareForFirstClass;
    }

    public double getBaseFareForBusinessClass() {
        return baseFareForBusinessClass;
    }

    public void setBaseFareForBusinessClass(double baseFareForBusinessClass) {
        this.baseFareForBusinessClass = baseFareForBusinessClass;
    }

    public double getBaseFareForEconomyClass() {
        return baseFareForEconomyClass;
    }

    public void setBaseFareForEconomyClass(double baseFareForEconomyClass) {
        this.baseFareForEconomyClass = baseFareForEconomyClass;
    }

    public int numberOfAvailableEconomyClassSeats(int totalBookedSeatsInEconomyClass) {
        return totalSeatsInEconomyClass - totalBookedSeatsInEconomyClass;
    }

    public int numberOfAvailableFirstClassSeats(int totalBookedSeatsInFirstClass) {
        return totalSeatsInFirstClass - totalBookedSeatsInFirstClass;
    }

    public int numberOfAvailableBussinessClassSeats(int totalBookedSeatsInBussinessClass) {
        return totalSeatsInBusinessClass - totalBookedSeatsInBussinessClass;
    }

    public int totalAvailableSeats(int totalBookedSeatsInEconomyClass,
                                   int totalBookedSeatsInFirstClass,
                                   int totalBookedSeatsInBusinessClass) {
        return numberOfAvailableBussinessClassSeats(totalBookedSeatsInBusinessClass) +
                numberOfAvailableEconomyClassSeats(totalBookedSeatsInEconomyClass) +
                numberOfAvailableFirstClassSeats(totalBookedSeatsInFirstClass);
    }

    public int totalSeats() {
        return totalSeatsInBusinessClass + totalSeatsInFirstClass + totalSeatsInEconomyClass;
    }

    public int getTotalAvailableSeats(int totalBookedSeatsInEconomyClass, int totalBookedSeatsInFirstClass, int totalBookedSeatsInBussinessClass) {
        return totalSeats() - (totalBookedSeatsInBussinessClass + totalBookedSeatsInEconomyClass + totalBookedSeatsInFirstClass);
    }

    @Override
    public String toString() {
        return "FlightData{" +
                "flightId=" + flightId +
                ", flightModel='" + flightModel + '\'' +
                ", totalSeatsInFirstClass=" + totalSeatsInFirstClass +
                ", totalSeatsInBusinessClass=" + totalSeatsInBusinessClass +
                ", totalSeatsInEconomyClass=" + totalSeatsInEconomyClass +
                ", baseFareForFirstClass=" + baseFareForFirstClass +
                ", baseFareForBusinessClass=" + baseFareForBusinessClass +
                ", baseFareForEconomyClass=" + baseFareForEconomyClass +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlightData that = (FlightData) o;
        return flightId == that.flightId &&
            totalSeatsInFirstClass == that.totalSeatsInFirstClass &&
            totalSeatsInBusinessClass == that.totalSeatsInBusinessClass &&
            totalSeatsInEconomyClass == that.totalSeatsInEconomyClass &&
            Double.compare(that.baseFareForFirstClass, baseFareForFirstClass) == 0 &&
            Double.compare(that.baseFareForBusinessClass, baseFareForBusinessClass) == 0 &&
            Double.compare(that.baseFareForEconomyClass, baseFareForEconomyClass) == 0 &&
            flightModel.equals(that.flightModel) &&
            source.equals(that.source) &&
            destination.equals(that.destination);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flightId, flightModel, source, destination, totalSeatsInFirstClass, totalSeatsInBusinessClass, totalSeatsInEconomyClass, baseFareForFirstClass, baseFareForBusinessClass, baseFareForEconomyClass);
    }
}
