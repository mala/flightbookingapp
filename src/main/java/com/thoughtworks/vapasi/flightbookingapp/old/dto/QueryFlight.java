package com.thoughtworks.vapasi.flightbookingapp.old.dto;

import java.util.Objects;

public class QueryFlight {
  private int flightId;
  private String name;
  private String source;
  private String destination;
  private int numOfTickets;
  private double totalFare;

  public QueryFlight(int flightId, String name, String source, String destination, int numOfTickets, double totalFare) {
    this.flightId = flightId;
    this.name = name;
    this.source = source;
    this.destination = destination;
    this.numOfTickets = numOfTickets;
    this.totalFare = totalFare;
  }

  public int getFlightId() {
    return flightId;
  }

  public void setFlightId(int flightId) {
    this.flightId = flightId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getNumOfTickets() {
    return numOfTickets;
  }

  public void setNumOfTickets(int numOfTickets) {
    this.numOfTickets = numOfTickets;
  }

  public double getTotalFare() {
    return totalFare;
  }

  public void setTotalFare(double totalFare) {
    this.totalFare = totalFare;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getDestination() {
    return destination;
  }

  public void setDestination(String destination) {
    this.destination = destination;
  }

  @Override
  public String toString() {
    return "QueryFlight{" +
        "flightId=" + flightId +
        ", name='" + name + '\'' +
        ", source='" + source + '\'' +
        ", destination='" + destination + '\'' +
        ", numOfTickets=" + numOfTickets +
        ", totalFare=" + totalFare +
        '}';
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    QueryFlight that = (QueryFlight) o;
    return flightId == that.flightId &&
        numOfTickets == that.numOfTickets &&
        Double.compare(that.totalFare, totalFare) == 0 &&
        Objects.equals(name, that.name) &&
        Objects.equals(source, that.source) &&
        Objects.equals(destination, that.destination);
  }

  @Override
  public int hashCode() {
    return Objects.hash(flightId, name, source, destination, numOfTickets, totalFare);
  }
}
