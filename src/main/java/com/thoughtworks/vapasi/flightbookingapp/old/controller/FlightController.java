package com.thoughtworks.vapasi.flightbookingapp.old.controller;

import com.thoughtworks.vapasi.flightbookingapp.old.model.FlightData;
import com.thoughtworks.vapasi.flightbookingapp.old.dto.QueryFlight;
import com.thoughtworks.vapasi.flightbookingapp.old.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
//@RequestMapping("flights")
public class FlightController {

    @Autowired
    FlightService flightService;

    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public List<FlightData> all() {
        return flightService.getAll();
    }

    @RequestMapping("/flights/search")
    public ResponseEntity<Map<String, List<QueryFlight>>> search(@RequestParam(value = "tickets", defaultValue = "1") int tickets,
                                                    @RequestParam(value = "src", required = false) String source,
                                                    @RequestParam(value = "des", required = false) String destination,
                                                    @RequestParam(value = "classType") int classType,
                                                    @RequestParam(value = "departureDate", required = false) String departureDate,
                                                    @RequestParam(value = "returnDate", required = false) String returnDate) {
        List<QueryFlight> flightsData = flightService.searchWithAllParameters(tickets, source, destination, classType, departureDate);
        List<QueryFlight> returnFlightData = flightService.searchWithAllParameters(tickets, destination, source, classType, returnDate);

        Map<String, List<QueryFlight>> results = new HashMap<>();
        results.put("onwards", flightsData);
        results.put("return", returnFlightData);

        return ResponseEntity.ok(results);
    }
}
