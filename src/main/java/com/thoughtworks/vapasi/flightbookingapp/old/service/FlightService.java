package com.thoughtworks.vapasi.flightbookingapp.old.service;

import com.thoughtworks.vapasi.flightbookingapp.Constants;
import com.thoughtworks.vapasi.flightbookingapp.old.dto.QueryFlight;
import com.thoughtworks.vapasi.flightbookingapp.old.model.ClassType;
import com.thoughtworks.vapasi.flightbookingapp.old.model.FlightData;
import com.thoughtworks.vapasi.flightbookingapp.old.repository.FlightDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


@Service
public class FlightService {
    private FlightDataRepository flightDataRepository;
    private BookingService bookingService;

    @Autowired
    public void setFlightRepository(FlightDataRepository flightDataRepository) {
        this.flightDataRepository = flightDataRepository;
    }

    @Autowired
    public void setBookingService(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    public List<FlightData> getAll() {
        return flightDataRepository.getAll();
    }

    public List<FlightData> search(int tickets, String source, String destination) {
        return getAll()
                .stream()
                .filter(flight -> flight.totalAvailableSeats(0, 0, 0) >= tickets
                    && flight.getSource().equals(source)
                    && flight.getDestination().equals(destination))
                .collect(Collectors.toList());
    }

    private List<FlightData> flightWithSourceDestinationAndClass(String source, String destination, ClassType type) {
        if (type == ClassType.BUSINESS_CLASS) {
            return getAll()
                .stream()
                .filter(flight -> flight.getTotalSeatsInBusinessClass() > 0
                    && flight.getSource().equals(source)
                    && flight.getDestination().equals(destination))
                .collect(Collectors.toList());
        }
        else if (type == ClassType.FIRST_CLASS) {
            return getAll()
                .stream()
                .filter(flight -> flight.getTotalSeatsInFirstClass() > 0
                    && flight.getSource().equals(source)
                    && flight.getDestination().equals(destination))
                .collect(Collectors.toList());
        }
        else if (type == ClassType.ECONOMY_CLASS) {
            return getAll()
                .stream()
                .filter(flight -> flight.getTotalSeatsInEconomyClass() > 0
                    && flight.getSource().equals(source)
                    && flight.getDestination().equals(destination))
                .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    public List<QueryFlight> searchWithAllParameters(int tickets, String source, String destination, int classType, String departureDate) {

        ClassType type = ClassType.fromCode(classType);

        //Get all the flights for the class type with source and destination
        List<FlightData> flightDataList = flightWithSourceDestinationAndClass(source,destination, type);

        //If now flights available for the given requirements
        if (flightDataList == null || flightDataList.size() == 0) return new ArrayList<>();

        Date dDate = new Date();
        try {
            dDate = new SimpleDateFormat(Constants.JSON_DATE_FORMAT).parse(departureDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (type == ClassType.FIRST_CLASS) {
            long diff = dDate.getTime() - new Date().getTime();
            int days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            if(days > 10) {
                return new ArrayList<>();
            }
        }

        List<FlightData> filteredFlightData = getFilteredListForSeatsAndDate(flightDataList, tickets, type, dDate);

        List<QueryFlight> queryFlightList = new ArrayList<>();
        for (FlightData flight: filteredFlightData) {
            int remainingTickets = bookingService.numberOfSeatsBooked(flight.getFlightId(), dDate, type);
            double totalFare = calculateTotalFare(type, flight, dDate, remainingTickets, tickets);
            queryFlightList.add(new QueryFlight(flight.getFlightId(), flight.getFlightModel(), flight.getSource(), flight.getDestination(), tickets, totalFare));
        }
        return queryFlightList;
    }

    private List<FlightData> getFilteredListForSeatsAndDate(List<FlightData> flightDataList, int tickets, ClassType type, Date dDate) {

        //Get remaining seats in all the filtered flights
        int remainingTickets = 0;
        List<FlightData> filteredFlightData = new ArrayList<FlightData>();

        //Filtering the flights based on remaining seats and date
        for (FlightData flightData : flightDataList) {
            remainingTickets = bookingService.numberOfSeatsBooked(flightData.getFlightId(), dDate, type);
            if (type == ClassType.ECONOMY_CLASS) {
                if (flightData.getTotalSeatsInEconomyClass() - remainingTickets > 0 && flightData.getTotalSeatsInEconomyClass() - remainingTickets >= tickets) {
                    filteredFlightData.add(flightData);
                }
            }
            else if (type == ClassType.BUSINESS_CLASS) {
                if (flightData.getTotalSeatsInBusinessClass() - remainingTickets > 0 && flightData.getTotalSeatsInBusinessClass() - remainingTickets >= tickets) {
                    filteredFlightData.add(flightData);
                }
            }
            else if (type == ClassType.FIRST_CLASS) {
                if (flightData.getTotalSeatsInFirstClass() - remainingTickets > 0 && flightData.getTotalSeatsInFirstClass() - remainingTickets >= tickets) {
                    filteredFlightData.add(flightData);
                }
            }
        }
        return filteredFlightData;
    }

    private double calculateTotalFare(ClassType type, FlightData flight, Date dateOfBooking, int remainingSeatsInEconomy, int ticketsToBeBooked) {
        double totalFare = 0;
        if (type == ClassType.ECONOMY_CLASS) {
            while (ticketsToBeBooked != 0) {
                totalFare += CalculationService.getCostOfEconomyClass(flight, remainingSeatsInEconomy);
                remainingSeatsInEconomy--;
                ticketsToBeBooked--;
            }
        }
        else if (type == ClassType.FIRST_CLASS) {
            totalFare = (double)ticketsToBeBooked * CalculationService.getCostOfFirstClass(flight, dateOfBooking);
        }
        else {
            totalFare = (double)ticketsToBeBooked * CalculationService.getCostOfBusinessClass(flight, dateOfBooking);
        }
        return totalFare;
    }
}
