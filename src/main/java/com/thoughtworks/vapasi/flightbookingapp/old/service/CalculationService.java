package com.thoughtworks.vapasi.flightbookingapp.old.service;

import com.thoughtworks.vapasi.flightbookingapp.old.model.FlightData;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class CalculationService {

  //Economy Class
  public static double getCostOfEconomyClass(FlightData flightData, int bookedSeatsInEconomyClass) {
    double ticketCost = flightData.getBaseFareForEconomyClass();
    double percentageOfSeatsLeft = ((flightData.getTotalSeatsInEconomyClass() - bookedSeatsInEconomyClass) * 100) / flightData.getTotalSeatsInEconomyClass();
    if (percentageOfSeatsLeft <= 10.00) {
      ticketCost += (ticketCost * 60) / 100;
      return ticketCost;
    }
    else if (percentageOfSeatsLeft <= 60.00 && percentageOfSeatsLeft > 10.00) {
      ticketCost += (ticketCost * 30) / 100;
      return ticketCost;
    }
    else if (percentageOfSeatsLeft > 60.00) {
      return ticketCost;
    }
    return ticketCost;
  }

  //Business Class
  public static double getCostOfBusinessClass(FlightData flightData, Date bookingDate) {
    double ticketCost = flightData.getBaseFareForBusinessClass();
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(bookingDate);
    int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
    if (dayOfWeek == 1 || dayOfWeek == 5 || dayOfWeek == 6) {
      ticketCost = ticketCost + (ticketCost / 100) * 40;
      return ticketCost;
    }
    return ticketCost;
  }

  //First Class
  public static double getCostOfFirstClass(FlightData flightData, Date dateOfBooking) {
    double ticketCost = flightData.getBaseFareForFirstClass();
    long diff = dateOfBooking.getTime() - new Date().getTime();
    int days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    if(days == 0) {
      ticketCost = ticketCost * 2;
    }
    else if (days < 10) {
      ticketCost += (ticketCost * ((10 - days) * 10)) / 100;
    }
    return ticketCost;
  }

}
