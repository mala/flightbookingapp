package com.thoughtworks.vapasi.flightbookingapp.old.model;

import java.util.Date;

public class Bookings {
  private int bookingId;
  private int flightID;
  private Date bookingDate;
  private ClassType seatType;
  private int numberOfSeatsBooked;

  public Bookings(int bookingId, int flightID, Date bookingDate, ClassType seatType, int numberOfSeatsBooked) {
    this.bookingId = bookingId;
    this.flightID = flightID;
    this.bookingDate = bookingDate;
    this.seatType = seatType;
    this.numberOfSeatsBooked = numberOfSeatsBooked;
  }

  public int getBookingId() {
    return bookingId;
  }

  public void setBookingId(int bookingId) {
    this.bookingId = bookingId;
  }

  public int getFlightID() {
    return flightID;
  }

  public void setFlightID(int flightID) {
    this.flightID = flightID;
  }

  public Date getBookingDate() {
    return bookingDate;
  }

  public void setBookingDate(Date bookingDate) {
    this.bookingDate = bookingDate;
  }

  public ClassType getSeatType() {
    return seatType;
  }

  public void setSeatType(ClassType seatType) {
    this.seatType = seatType;
  }

  public int getNumberOfSeatsBooked() {
    return numberOfSeatsBooked;
  }

  public void setNumberOfSeatsBooked(int numberOfSeatsBooked) {
    this.numberOfSeatsBooked = numberOfSeatsBooked;
  }
}
