package com.thoughtworks.vapasi.flightbookingapp.old.model;

import java.util.HashMap;
import java.util.Map;

public enum ClassType {
    ECONOMY_CLASS(0),
    FIRST_CLASS(1),
    BUSINESS_CLASS(2);

    private static final Map<Integer, ClassType> REGISTRY = new HashMap<>();

    static {
        for (ClassType instance : ClassType.values()) {
            REGISTRY.put(instance.levelCode, instance);
        }
    }

    private final int levelCode;

    ClassType(int levelCode) {
      this.levelCode = levelCode;
    }

    public static ClassType fromCode(int value) {
        return REGISTRY.get(value);
    }
}
