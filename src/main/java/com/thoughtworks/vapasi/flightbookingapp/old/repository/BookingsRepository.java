package com.thoughtworks.vapasi.flightbookingapp.old.repository;

import com.thoughtworks.vapasi.flightbookingapp.Constants;
import com.thoughtworks.vapasi.flightbookingapp.old.model.Bookings;
import com.thoughtworks.vapasi.flightbookingapp.old.model.ClassType;
import com.thoughtworks.vapasi.flightbookingapp.newmodel.repository.ObjectRepository;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Repository
public class BookingsRepository implements ObjectRepository<Bookings> {

  private Map<Integer, Bookings> repository;

  public BookingsRepository() {
    this.repository = new HashMap<>();

    Date dDate = new Date();
    try {
      dDate = new SimpleDateFormat(Constants.DATE_FORMAT).parse("10/09/2019");
    } catch (ParseException e) {
      e.printStackTrace();
    }
    this.store(new Bookings(1, 1, dDate, ClassType.ECONOMY_CLASS, 20));
    this.store(new Bookings(2, 1, dDate, ClassType.ECONOMY_CLASS, 40));
    this.store(new Bookings(3, 1, dDate, ClassType.BUSINESS_CLASS, 7));
    this.store(new Bookings(4, 4, dDate, ClassType.ECONOMY_CLASS, 8));
    this.store(new Bookings(5, 4, dDate, ClassType.ECONOMY_CLASS, 4));
    this.store(new Bookings(6, 6, dDate, ClassType.ECONOMY_CLASS, 5));
    this.store(new Bookings(7, 6, dDate, ClassType.FIRST_CLASS, 9));
    this.store(new Bookings(8, 6, dDate, ClassType.ECONOMY_CLASS, 10));

    try {
      dDate = new SimpleDateFormat(Constants.DATE_FORMAT).parse("20/09/2019");
    } catch (ParseException e) {
      e.printStackTrace();
    }

    this.store(new Bookings(9, 7, dDate, ClassType.ECONOMY_CLASS, 4));
    this.store(new Bookings(10, 7, dDate, ClassType.ECONOMY_CLASS, 4));
    this.store(new Bookings(11, 7, dDate, ClassType.BUSINESS_CLASS, 7));
    this.store(new Bookings(12, 10, dDate, ClassType.ECONOMY_CLASS, 8));
    this.store(new Bookings(13, 10, dDate, ClassType.ECONOMY_CLASS, 4));
    this.store(new Bookings(14, 12, dDate, ClassType.ECONOMY_CLASS, 5));
    this.store(new Bookings(15, 12, dDate, ClassType.FIRST_CLASS, 9));
    this.store(new Bookings(16, 12, dDate, ClassType.ECONOMY_CLASS, 10));

    try {
      dDate = new SimpleDateFormat(Constants.DATE_FORMAT).parse("2/09/2019");
    } catch (ParseException e) {
      e.printStackTrace();
    }
    this.store(new Bookings(17, 1, dDate, ClassType.ECONOMY_CLASS, 20));
    this.store(new Bookings(18, 1, dDate, ClassType.ECONOMY_CLASS, 40));
    this.store(new Bookings(19, 1, dDate, ClassType.BUSINESS_CLASS, 7));
    this.store(new Bookings(20, 4, dDate, ClassType.ECONOMY_CLASS, 8));
    this.store(new Bookings(21, 4, dDate, ClassType.ECONOMY_CLASS, 4));
    this.store(new Bookings(22, 6, dDate, ClassType.ECONOMY_CLASS, 5));
    this.store(new Bookings(23, 6, dDate, ClassType.FIRST_CLASS, 9));
    this.store(new Bookings(24, 6, dDate, ClassType.ECONOMY_CLASS, 10));

  }

  @Override
  public void store(Bookings bookings) {
    repository.put(bookings.getBookingId(), bookings);
  }

  @Override
  public Bookings retrieve(int id) {
    return null;
  }

  @Override
  public Bookings search(String name) {
    return null;
  }

  @Override
  public Bookings delete(int id) {
    return null;
  }

  @Override
  public List<Bookings> getAll() {
    return new ArrayList<>(repository.values());
  }

  public int getMaxBookingId() {
    Comparator<Bookings> comparator = Comparator.comparing( Bookings::getBookingId);
    Bookings maxObject = getAll().stream().max(comparator).get();
    return maxObject.getBookingId();
  }
}
