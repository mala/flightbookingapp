package com.thoughtworks.vapasi.flightbookingapp.old.service;

import com.thoughtworks.vapasi.flightbookingapp.old.model.Bookings;
import com.thoughtworks.vapasi.flightbookingapp.old.model.ClassType;
import com.thoughtworks.vapasi.flightbookingapp.old.repository.BookingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookingService {
  private BookingsRepository bookingsRepository;

  @Autowired
  public void setBookingsRepository(BookingsRepository bookingsRepository) {
    this.bookingsRepository = bookingsRepository;
  }

  public int numberOfSeatsBooked(int flightId, Date dateOfBooking, ClassType type) {
    List<Bookings> filteredBookings = bookingsRepository.getAll()
        .stream()
        .filter(bookings -> bookings.getFlightID() == flightId
            && bookings.getBookingDate().equals(dateOfBooking)
            && bookings.getSeatType() == type)
        .collect(Collectors.toList());

    return filteredBookings.stream().mapToInt(bookings -> bookings.getNumberOfSeatsBooked()).sum();
  }
}
