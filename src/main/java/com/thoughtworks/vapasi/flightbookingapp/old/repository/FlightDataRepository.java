package com.thoughtworks.vapasi.flightbookingapp.old.repository;

import com.thoughtworks.vapasi.flightbookingapp.old.model.FlightData;
import com.thoughtworks.vapasi.flightbookingapp.newmodel.repository.ObjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class FlightDataRepository implements ObjectRepository<FlightData> {

    @Autowired
    public JdbcTemplate jdbcTemplate;
    private Map<Integer, FlightData> repository;

    public FlightDataRepository() {
        this.repository = new HashMap<>();

        this.store(new FlightData(1,"Boeing 777- 200LR(77L)", "Hyderabad", "Delhi", 40, 10, 100, 10000, 25000, 6000));
        this.store(new FlightData(2,"Airbus A319 V2", "Hyderabad", "Delhi", 10, 10, 10, 10000, 25000, 6000));
        this.store(new FlightData(3,"Airbus A321", "Hyderabad", "Delhi", 3, 10, 40, 10000, 25000, 6000));

        this.store(new FlightData(4,"Boeing 777- 200LR(77L)", "Hyderabad", "Pune", 40, 10, 100, 10000, 25000, 6000));
        this.store(new FlightData(5,"Airbus A319 V2", "Hyderabad", "Pune", 10, 10, 10, 10000, 25000, 6000));
        this.store(new FlightData(6,"Airbus A321", "Hyderabad", "Pune", 3, 10, 40, 10000, 25000, 6000));

        this.store(new FlightData(7,"Boeing 777- 200LR(77L)", "Hyderabad", "Mumbai", 40, 10, 100, 10000, 25000, 6000));
        this.store(new FlightData(8,"Airbus A319 V2", "Hyderabad", "Mumbai", 10, 10, 10, 10000, 25000, 6000));
        this.store(new FlightData(9,"Airbus A321", "Hyderabad", "Mumbai", 3, 10, 40, 10000, 25000, 6000));

        this.store(new FlightData(7,"Boeing 777- 200LR(77L)", "Delhi", "Hyderabad", 40, 10, 100, 10000, 25000, 6000));
        this.store(new FlightData(8,"Airbus A319 V2", "Delhi", "Hyderabad", 10, 10, 10, 10000, 25000, 6000));
        this.store(new FlightData(9,"Airbus A321", "Delhi", "Hyderabad", 3, 10, 40, 10000, 25000, 6000));

        this.store(new FlightData(10,"Boeing 777- 200LR(77L)", "Pune", "Hyderabad", 40, 10, 100, 10000, 25000, 6000));
        this.store(new FlightData(11,"Airbus A319 V2", "Pune", "Hyderabad", 10, 10, 10, 10000, 25000, 6000));
        this.store(new FlightData(12,"Airbus A321", "Pune", "Hyderabad", 3, 10, 40, 10000, 25000, 6000));

        this.store(new FlightData(13,"Boeing 777- 200LR(77L)", "Mumbai", "Hyderabad", 40, 10, 100, 10000, 25000, 6000));
        this.store(new FlightData(14,"Airbus A319 V2", "Mumbai", "Hyderabad", 10, 10, 10, 10000, 25000, 6000));
        this.store(new FlightData(15,"Airbus A321", "Mumbai", "Hyderabad", 3, 10, 40, 10000, 25000, 6000));

    }


    @Override
    public void store(FlightData flightData) {
        repository.put(flightData.getFlightId(), flightData);
    }

    @Override
    public FlightData retrieve(int id) {
        return repository.get(id);
    }

    @Override
    public FlightData search(String name) {
        Collection<FlightData> flightData = repository.values();
        for (FlightData fData : flightData) {
            if (fData.getFlightModel().equalsIgnoreCase(name))
                return fData;
        }
        return null;
    }

    @Override
    public FlightData delete(int id) {
        FlightData f = repository.get(id);
        this.repository.remove(id);
        return f;
    }

    @Override
    public List<FlightData> getAll() {
        String sql = "select * from flight;";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql);
        for (Map<String,Object> entry: maps) {
           // FlightData flight = new FlightData((Int)entry.get("flightId"));

            System.out.println(entry.get("flightId"));
            System.out.println(entry.get("flightName"));
            System.out.println(entry.get("flightDateTime"));
        }
        return new ArrayList<>(repository.values());
    }
}
