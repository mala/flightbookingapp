package com.thoughtworks.vapasi.flightbookingapp.service;

import com.thoughtworks.vapasi.flightbookingapp.Constants;
import com.thoughtworks.vapasi.flightbookingapp.old.model.Bookings;
import com.thoughtworks.vapasi.flightbookingapp.old.model.ClassType;
import com.thoughtworks.vapasi.flightbookingapp.old.repository.BookingsRepository;
import com.thoughtworks.vapasi.flightbookingapp.old.service.BookingService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BookingServiceTest {
  @Mock
  BookingsRepository bookingsRepository;

  BookingService bookingService;

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);
    bookingService = new BookingService();
    bookingService.setBookingsRepository(bookingsRepository);
  }

  @Test
  public void shouldReturnAvailableSeatsForEconomyClass() {

    List<Bookings> bookings = new ArrayList<Bookings>();
    SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT);
    Date bDate = new Date();
    try {
      bDate = format.parse("28-01-2020");
    } catch (ParseException e) {
      e.printStackTrace();
    }
    bookings.add(new Bookings(1, 1, bDate, ClassType.ECONOMY_CLASS, 4));
    bookings.add(new Bookings(2, 1, bDate, ClassType.ECONOMY_CLASS, 4));
    bookings.add(new Bookings(3, 1, bDate, ClassType.BUSINESS_CLASS, 7));
    bookings.add(new Bookings(4, 4, bDate, ClassType.ECONOMY_CLASS, 8));
    bookings.add(new Bookings(5, 4, bDate, ClassType.ECONOMY_CLASS, 4));
    bookings.add(new Bookings(6, 6, bDate, ClassType.ECONOMY_CLASS, 5));
    when(bookingsRepository.getAll()).thenReturn(bookings);

    int expectedResult = 8;
    int actualResult = bookingService.numberOfSeatsBooked(1, bDate, ClassType.ECONOMY_CLASS);
    assertEquals(expectedResult, actualResult);
  }
}
