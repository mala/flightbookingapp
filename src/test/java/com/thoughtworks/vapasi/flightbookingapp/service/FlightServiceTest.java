package com.thoughtworks.vapasi.flightbookingapp.service;

import com.thoughtworks.vapasi.flightbookingapp.Constants;
import com.thoughtworks.vapasi.flightbookingapp.old.dto.QueryFlight;
import com.thoughtworks.vapasi.flightbookingapp.old.model.ClassType;
import com.thoughtworks.vapasi.flightbookingapp.old.model.FlightData;
import com.thoughtworks.vapasi.flightbookingapp.old.repository.FlightDataRepository;
import com.thoughtworks.vapasi.flightbookingapp.old.service.BookingService;
import com.thoughtworks.vapasi.flightbookingapp.old.service.FlightService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FlightServiceTest {
    @Mock
    FlightDataRepository flightDataRepository;

    @Mock
    BookingService bookingService;

    private FlightService flightService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        flightService = new FlightService();
        flightService.setFlightRepository(flightDataRepository);
        flightService.setBookingService(bookingService);
    }

    @Test
    public void shouldReturnNoFlightsWhenNoMatchingIsFound() {
        when(flightDataRepository.getAll()).thenReturn(new ArrayList<>());

        List<FlightData> expectedResult = new ArrayList<>();
        List<FlightData> actualResult = flightService.search(10, "Hyderabad", "Delhi");
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void searchTest() {
        List<FlightData> flightData = new ArrayList<>();
        flightData.add(new FlightData(1,"Boeing 777-200LR(77L)", "Hyderabad","Delhi", 40, 10, 100, 10000, 25000, 6000));
        flightData.add(new FlightData(2,"Airbus A319 V2", "Hyderabad","Delhi",10, 10, 10, 10000, 25000, 6000));
        flightData.add(new FlightData(3,"Airbus A321", "Hyderabad","Delhi", 40, 10, 40, 10000, 25000, 6000));
        when(flightDataRepository.getAll()).thenReturn(flightData);

        List<FlightData> expectedResult = new ArrayList<FlightData>(flightData);
        List<FlightData> actualResult = flightService.search(10, "Hyderabad", "Delhi");
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void search1Test() {
        List<FlightData> flightData = new ArrayList<>();
        FlightData f1 = new FlightData(1,"Boeing 777-200LR(77L)","Hyderabad","Delhi", 40, 10, 100, 10000, 25000, 6000);
        FlightData f2 = new FlightData(2,"Airbus A319 V2","Hyderabad","Delhi", 10, 10, 10, 10000, 25000, 6000);
        FlightData f3 = new FlightData(3,"Airbus A321","Hyderabad","Mumbai", 4, 2, 4, 10000, 25000, 6000);

        flightData.add(f1);
        flightData.add(f2);
        flightData.add(f3);

        when(flightDataRepository.getAll()).thenReturn(flightData);

        List<FlightData> expectedResult = new ArrayList<FlightData>();
        expectedResult.add(f1);
        expectedResult.add(f2);
        List<FlightData> actualResult = flightService.search(20, "Hyderabad", "Delhi");
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void searchFlightWhenNumberOfTicketsIsOne() {
        List<FlightData> flightData = new ArrayList<>();
        FlightData f1 = new FlightData(1,"Boeing 777-200LR(77L)","Hyderabad","Delhi", 40, 10, 100, 10000, 25000, 6000);
        FlightData f2 = new FlightData(2,"Airbus A319 V2","Hyderabad","Delhi", 10, 10, 10, 10000, 25000, 6000);
        FlightData f3 = new FlightData(3,"Airbus A321","Hyderabad","Delhi", 4, 2, 4, 10000, 25000, 6000);

        flightData.add(f1);
        flightData.add(f2);
        flightData.add(f3);

        when(flightDataRepository.getAll()).thenReturn(flightData);

        List<FlightData> expectedResult = new ArrayList<FlightData>();
        expectedResult.add(f1);
        expectedResult.add(f2);
        expectedResult.add(f3);
        List<FlightData> actualResult = flightService.search(1, "Hyderabad", "Delhi");
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void searchWithAllParametersTest()  {
        List<FlightData> flightData = new ArrayList<FlightData>();
        flightData.add(new FlightData(1,"Boeing 777-200LR(77L)", "Hyderabad","Delhi", 40, 10, 100, 10000, 25000, 6000));
        flightData.add(new FlightData(2,"Airbus A319 V2", "Hyderabad","Delhi",10, 10, 10, 10000, 25000, 6000));
        flightData.add(new FlightData(3,"Airbus A321", "Hyderabad","Delhi", 40, 10, 40, 10000, 25000, 6000));
        when(flightDataRepository.getAll()).thenReturn(flightData);
        int tickets = 5;
        String source = "Hyderabad";
        String dest = "Delhi";
        int classType = 0;
        String dDate = "2019-08-31";
        Date bDate = null;
        try {
            bDate = new SimpleDateFormat(Constants.JSON_DATE_FORMAT).parse(dDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        when(bookingService.numberOfSeatsBooked(1, bDate, ClassType.fromCode(classType))).thenReturn(6);
        when(bookingService.numberOfSeatsBooked(2, bDate, ClassType.fromCode(classType))).thenReturn(6);
        when(bookingService.numberOfSeatsBooked(3, bDate, ClassType.fromCode(classType))).thenReturn(6);

        List<FlightData> result = flightData.stream()
            .filter(flight -> flight.getTotalSeatsInEconomyClass() - 6 >= tickets)
            .collect(Collectors.toList());

        List<QueryFlight> expectedResult = new ArrayList<QueryFlight>();
        double totalFare = 0;
        for (FlightData flight: result) {
            totalFare = tickets * flight.getBaseFareForEconomyClass();
            expectedResult.add(new QueryFlight(flight.getFlightId(), flight.getFlightModel(), flight.getSource(), flight.getDestination(), tickets, totalFare));
        }

        List<QueryFlight> actualResult = flightService.searchWithAllParameters(tickets, source, dest, classType, dDate);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void searchWithNo() {
        List<FlightData> flightData = new ArrayList<>();
        flightData.add(new FlightData(1,"Boeing 777-200LR(77L)", "Hyderabad","Delhi", 40, 10, 100, 10000, 25000, 6000));
        flightData.add(new FlightData(2,"Airbus A319 V2", "Hyderabad","Delhi",10, 10, 20, 10000, 25000, 6000));
        flightData.add(new FlightData(3,"Airbus A321", "Hyderabad","Delhi", 40, 10, 40, 10000, 25000, 6000));
        when(flightDataRepository.getAll()).thenReturn(flightData);
        int tickets = 5;
        String source = "Hyderabad";
        String dest = "Delhi";
        int classType = 0;
        String dDate = "2019-08-31";
        Date bDate = null;
        try {
            bDate = new SimpleDateFormat(Constants.JSON_DATE_FORMAT).parse(dDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        when(bookingService.numberOfSeatsBooked(1, bDate, ClassType.fromCode(classType))).thenReturn(6);
        when(bookingService.numberOfSeatsBooked(2, bDate, ClassType.fromCode(classType))).thenReturn(6);
        when(bookingService.numberOfSeatsBooked(3, bDate, ClassType.fromCode(classType))).thenReturn(6);

        List<FlightData> result = flightData.stream()
            .filter(flight -> flight.getTotalSeatsInEconomyClass() - 6 >= tickets)
            .collect(Collectors.toList());

        List<QueryFlight> expectedResult = new ArrayList<QueryFlight>();
        double totalFare = 0;
        for (FlightData flight: result) {
            totalFare = tickets * flight.getBaseFareForEconomyClass();
            expectedResult.add(new QueryFlight(flight.getFlightId(), flight.getFlightModel(), flight.getSource(), flight.getDestination(), tickets, totalFare));
        }

        List<QueryFlight> actualResult = flightService.searchWithAllParameters(tickets, source, dest, classType, dDate);

        assertEquals(expectedResult, actualResult);
    }


}
