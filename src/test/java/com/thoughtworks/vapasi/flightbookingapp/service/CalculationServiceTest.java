package com.thoughtworks.vapasi.flightbookingapp.service;

import com.thoughtworks.vapasi.flightbookingapp.Constants;
import com.thoughtworks.vapasi.flightbookingapp.old.model.FlightData;
import com.thoughtworks.vapasi.flightbookingapp.old.service.CalculationService;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CalculationServiceTest {
  @Test
  public void shouldReturnFareForEconomyClassWhenAvailableSeatsAre50Percent() {
    FlightData f1 = new FlightData(1,"Boeing 777-200LR(77L)","Hyderabad","Delhi", 40, 10, 100, 10000, 25000, 6000);
    double expectedTicketCost = f1.getBaseFareForEconomyClass() + ((f1.getBaseFareForEconomyClass() * 30) / 100);
    double actualResult = CalculationService.getCostOfEconomyClass(f1, 50);

    assertEquals(expectedTicketCost, actualResult);
  }

  @Test
  public void shouldReturnFareForEconomyClassWhenAvailableSeatsAreLessThan10Percent() {
    FlightData f1 = new FlightData(1,"Boeing 777-200LR(77L)","Hyderabad","Delhi", 40, 10, 100, 10000, 25000, 6000);
    double expectedTicketCost = f1.getBaseFareForEconomyClass() + ((f1.getBaseFareForEconomyClass() * 60) / 100);
    double actualResult = CalculationService.getCostOfEconomyClass(f1, 92);

    assertEquals(expectedTicketCost, actualResult);
  }

  @Test
  public void shouldReturnFareForEconomyClassWhenAvailableSeatsAreMoreThan60Percent() {
    FlightData f1 = new FlightData(1,"Boeing 777-200LR(77L)","Hyderabad","Delhi", 40, 10, 100, 10000, 25000, 6000);
    double expectedTicketCost = f1.getBaseFareForEconomyClass();
    double actualResult = CalculationService.getCostOfEconomyClass(f1, 10);

    assertEquals(expectedTicketCost, actualResult);
  }

  @Test
  public void shouldReturnFareForBusinessClassWhenItsWeekend() {
    FlightData f1 = new FlightData(1,"Boeing 777-200LR(77L)","Hyderabad","Delhi", 40, 10, 100, 10000, 25000, 6000);
    double expectedTicketCost = f1.getBaseFareForBusinessClass() + ((f1.getBaseFareForBusinessClass() / 100) * 40);
    String sDate1="16/08/2019";
    SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT);
    Date date = new Date();
    try {
      date = format.parse(sDate1);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    double actualResult = CalculationService.getCostOfBusinessClass(f1, date);
    assertEquals(expectedTicketCost, actualResult);
  }

  @Test
  public void shouldReturnFareForBusinessClassWhenItsWeekday() {
    FlightData f1 = new FlightData(1,"Boeing 777-200LR(77L)","Hyderabad","Delhi", 40, 10, 100, 10000, 25000, 6000);
    double expectedTicketCost = f1.getBaseFareForBusinessClass();
    String sDate1="19/08/2019";
    SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT);
    Date date = new Date();
    try {
      date = format.parse(sDate1);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    double actualResult = CalculationService.getCostOfBusinessClass(f1, date);
    assertEquals(expectedTicketCost, actualResult);
  }

  @Test
  public void shouldReturnFareForFirstClassWhenDiffIsZero() {
    FlightData f1 = new FlightData(1,"Boeing 777-200LR(77L)","Hyderabad","Delhi", 40, 10, 100, 10000, 25000, 6000);
    double expectedTicketCost = f1.getBaseFareForFirstClass() + ((f1.getBaseFareForFirstClass() * 100)/100);

    SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT);
    Calendar cal = Calendar.getInstance();

    cal.add(Calendar.DAY_OF_MONTH, 0);
    String newDate = format.format(cal.getTime());
    Date bookingDate = new Date();
    try {
      bookingDate = format.parse(newDate);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    double actualResult = CalculationService.getCostOfFirstClass(f1, bookingDate);
    assertEquals(expectedTicketCost, actualResult);
  }

  @Test
  public void shouldReturnFareForFirstClassWhenDiffIsOne() {
    FlightData f1 = new FlightData(1,"Boeing 777-200LR(77L)","Hyderabad","Delhi", 40, 10, 100, 10000, 25000, 6000);
    double expectedTicketCost = f1.getBaseFareForFirstClass() + ((f1.getBaseFareForFirstClass() * 100)/100);

    SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT);
    Calendar cal = Calendar.getInstance();

    cal.add(Calendar.DAY_OF_MONTH, 1);
    String newDate = format.format(cal.getTime());
    Date bookingDate = new Date();
    try {
      bookingDate = format.parse(newDate);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    double actualResult = CalculationService.getCostOfFirstClass(f1, bookingDate);
    assertEquals(expectedTicketCost, actualResult);
  }

  @Test
  public void shouldReturnFareForFirstClassForOpeningDate() {
    FlightData f1 = new FlightData(1,"Boeing 777-200LR(77L)","Hyderabad","Delhi", 40, 10, 100, 10000, 25000, 6000);
    double expectedTicketCost = f1.getBaseFareForFirstClass();

    SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT);
    Calendar cal = Calendar.getInstance();

    cal.add(Calendar.DAY_OF_MONTH, 11);
    String newDate = format.format(cal.getTime());
    Date bookingDate = new Date();
    try {
      bookingDate = format.parse(newDate);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    double actualResult = CalculationService.getCostOfFirstClass(f1, bookingDate);
    assertEquals(expectedTicketCost, actualResult);
  }

  @Test
  public void shouldReturnFareForFirstClassWhenDiffIsFive() {
    FlightData f1 = new FlightData(1,"Boeing 777-200LR(77L)","Hyderabad","Delhi", 40, 10, 100, 10000, 25000, 6000);
    double expectedTicketCost = f1.getBaseFareForFirstClass() + ((f1.getBaseFareForFirstClass() * 60)/100);

    SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT);
    Calendar cal = Calendar.getInstance();

    cal.add(Calendar.DAY_OF_MONTH, 5);
    String newDate = format.format(cal.getTime());
    Date bookingDate = new Date();
    try {
      bookingDate = format.parse(newDate);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    double actualResult = CalculationService.getCostOfFirstClass(f1, bookingDate);
    assertEquals(expectedTicketCost, actualResult);
  }

}
